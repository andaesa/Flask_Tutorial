# API error handler for status code 403
def forbidden(message):
	response = jsonify({'error': 'forbidden', 'message': message})
	response.status_code = 403
	return response

# API error handler for ValidationError exception
@api.errorhandler(ValidationError)
def validation_error(e):
	return bad_request(e.args[0])
	